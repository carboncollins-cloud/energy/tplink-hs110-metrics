job "metrics-energy-tplink-hs110" {
  name = "Energy monitoring (TP-Link HS110)"
  type = "service"
  region = "[[ .defaultRegion ]]"
  datacenters = ["[[ .defaultDatacenter ]]"]
  priority = "[[ .defaultPriority ]]"
  namespace = "energy"

  group "office-1" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "tplink-hs110-metrics"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "computer" {
      driver = "docker"

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      config {
        image = "[[ .jobDockerImage ]]"
      }

      resources {
        cpu = 20
        memory = 50
      }

      vault {
        policies = ["energy-monitor"]
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        PUID = "[[ .defaultUserId ]]"
        PGID = "[[ .defaultGroupId ]]"
      }

      template {
        data = <<EOH
          INFLUXDB_HOST=http://{{ env "NOMAD_UPSTREAM_ADDR_influxdb" }}
          INFLUXDB_BUCKET=Energy

          {{ with secret "energy-monitor/office-1/computer" }}
          INFLUXDB_ORG={{ index .Data.data "INFLUXDB_ORG" }}
          INFLUXDB_TOKEN={{ index .Data.data "INFLUXDB_TOKEN" }}
          DEVICE_IP={{ index .Data.data "DEVICE_IP" }}
          DEVICE_ALIAS=Office 1 Computer
          {{ end }}
        EOH

        destination = "secrets/monitor.env"
        env = true
      }
    }
  }

  group "server-rack-1" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "tplink-hs110-metrics"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "rack-1" {
      driver = "docker"

      config {
        image = "[[ .jobDockerImage ]]"
      }

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      resources {
        cpu = 20
        memory = 50
      }

      vault {
        policies = ["energy-monitor"]
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        PUID = "[[ .defaultUserId ]]"
        PGID = "[[ .defaultGroupId ]]"
      }

      template {
        data = <<EOH
          INFLUXDB_HOST=http://{{ env "NOMAD_UPSTREAM_ADDR_influxdb" }}
          INFLUXDB_BUCKET=Energy

          {{ with secret "energy-monitor/server-rack-1" }}
          INFLUXDB_ORG={{ index .Data.data "INFLUXDB_ORG" }}
          INFLUXDB_TOKEN={{ index .Data.data "INFLUXDB_TOKEN" }}
          DEVICE_IP={{ index .Data.data "DEVICE_IP" }}
          DEVICE_ALIAS=Server Rack 1
          {{ end }}
        EOH

        destination = "secrets/monitor.env"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
