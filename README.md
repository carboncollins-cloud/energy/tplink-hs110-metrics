# Energy Monitoring - TP-Link HS110

[[_TOC_]]

## Description

Energy monitoring by pulling power usage from [TP-Link HS110](https://www.tp-link.com/se/home-networking/smart-plug/hs110/) and forwarding it to an InfluxDB bucket.
